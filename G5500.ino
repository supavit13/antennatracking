#include <SPI.h>
#include "G5500.h"
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"
#include <StandardCplusplus.h>
#include <vector>
using namespace std;
G5500 rotor = G5500();
File myFile;
RTC_DS3231 rtc;
String tempPayload = "";
String buff;
String timeserver = "#";
String AOS = "";
String LOS = "";
int faz = 0;
int fel = 0;
vector<String> time_vector;
vector<int> az_vector;
vector<int> el_vector;
int n = 0;
int mode = 0;
int stepCaribration = 0;

void printTime() {
  DateTime now = rtc.now();
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(" ");
  Serial.print(now.hour() + 0, DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
}
String getCurrentTime() {
  DateTime now = rtc.now();
  String hh = String(now.hour() + 0);
  if (hh.length() == 1) hh = "0" + hh;
  String mm = String(now.minute());
  if (mm.length() == 1) mm = "0" + mm;
  String ss = String(now.second());
  if (ss.length() == 1) ss = "0" + ss;
  return hh + ":" + mm + ":" + ss;
}
void rotation() {
  if (mode == 2 ) {
    myFile = SD.open("test.txt");
    if (myFile) {
      while (myFile.available()) {
        buff = myFile.readStringUntil('\n');
        if ((char)buff[0] == 'E') break;
        int Az = buff.substring(0, buff.indexOf(',')).toInt();
        int El = buff.substring(buff.indexOf(',') + 1, buff.lastIndexOf(',')).toInt();
        String Time = buff.substring(buff.lastIndexOf(',') + 1, buff.indexOf('#'));
        if (faz == 0 && fel == 0) {
          faz = Az;
          fel = El;
          rotor.setAzEl(faz, fel);
        }
        //        az_vector.push_back(Az);
        //        el_vector.push_back(El);
        //        time_vector.push_back(Time);
        if (getCurrentTime() == Time) {

          Serial.print(Az);
          Serial.print("," + String(El) + '\n');
          Serial.println(Time);
          rotor.setAzEl(Az, El);
          break;
        }
      }
      Serial.println("end of file");
      //      mode = 7;
      myFile.close();
      delay(1000);

    } else {
      Serial.println("error opening test.txt");
    }
  } else {
    for (int i = 0; i < time_vector.size(); i++) {
      //    Serial.println(getCurrentTime());
      if (getCurrentTime() == time_vector[i]) {
        Serial.print(az_vector[i]);
        Serial.print("," + String(el_vector[i]) + '\n');
        Serial.println(time_vector[i]);
        rotor.setAzEl(az_vector[i], el_vector[i]);
        break;
      }
    }
    delay(1000);
  }

}
void systemcheck() {
  Serial.println("============== System check ==============");
  Serial.print("RTC time is ");
  printTime();
  if (SD.begin(4)) Serial.println("SD card OK!");
  else Serial.println("SD card Fail!");
  Serial.print("Azimuth degrees >> ");
  Serial.print(rotor.getAzDegrees());
  Serial.print(" and Digital number >> ");
  Serial.println(rotor.getAz());
  Serial.print("Elevation degrees >> ");
  Serial.print(rotor.getElDegrees());
  Serial.print(" and Digital number >> ");
  Serial.println(rotor.getEl());
}
void showmenu() {
  Serial.println("============== Mode ==============");
  Serial.println("1) Manual input Azimuth and Elevation");
  Serial.println("2) Read task from SDCard");
  Serial.println("3) System check");
  Serial.println("4) Show current task");
  Serial.println("5) Remove current task");
  Serial.println("6) Adjust datetime");
  Serial.println("7) Caribration Yaesu G5500");
  Serial.println("Please enter number for choose operation mode");
}

void RTCinitializting() {
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
  }
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    //    mode = 99;
    //    MQTTconnection();
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
}

void SDinitializing() {

  if (!SD.begin(4));
  //  if (SD.exists("test.txt")) {
  //    SD.remove("test.txt");
  //  }
  myFile = SD.open("test.txt");
  if (myFile) {
    Serial.println("SD initialization done.");
  } else {
    Serial.println("SD initialization failed!");
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Wait for initialization system. . .");
  RTCinitializting();
  SDinitializing();
  digitalWrite(rotor.LEDAz_P, HIGH);
  digitalWrite(rotor.LEDAz_M, HIGH);
  digitalWrite(rotor.LEDEl_P, HIGH);
  digitalWrite(rotor.LEDEl_M, HIGH);
  delay(1000);
  digitalWrite(rotor.LEDAz_P, LOW);
  delay(200);
  digitalWrite(rotor.LEDAz_M, LOW);
  delay(200);
  digitalWrite(rotor.LEDEl_P, LOW);
  delay(200);
  digitalWrite(rotor.LEDEl_M, LOW);
  delay(200);
  showmenu();
}
void removeTask() {
  tempPayload = "";
  AOS = "";
  LOS = "";
  faz = 0;
  fel = 0;
  n = 0;
  time_vector.clear();
  el_vector.clear();
  az_vector.clear();
  SDinitializing();
  mode = 0;
}
void showTask() {
  if (mode == 2) {
    myFile = SD.open("test.txt");
    if (myFile) {
      while (myFile.available()) {
        String buffe = myFile.readStringUntil('\n');
        if ((char)buffe[0] == 'E') break;
        int Az = buffe.substring(0, buffe.indexOf(',')).toInt();
        int El = buffe.substring(buffe.indexOf(',') + 1, buffe.lastIndexOf(',')).toInt();
        String Time = buffe.substring(buffe.lastIndexOf(',') + 1, buffe.indexOf('#'));
        Serial.print(Az);
        Serial.print("," + String(El) + ',');
        Serial.println(Time);

        if (getCurrentTime() == Time) {

          Serial.print(Az);
          Serial.print("," + String(El) + '\n');
          Serial.println(Time);
          rotor.setAzEl(Az, El);
          break;
        }
      }
      myFile.close();
      delay(1000);
    }
  }
  else if (time_vector.size() == 0) {
    Serial.println("empty");
  } else if (AOS != "") {
    Serial.println("AOS is " + AOS);
    Serial.println("LOS is " + LOS);
  }
  for (int i = 0; i < time_vector.size(); i++) {
    Serial.print(az_vector[i]);
    Serial.print(",");
    Serial.print(el_vector[i]);
    Serial.print(",");
    Serial.println(time_vector[i]);

  }
}
void adjustdatetime() {
  int years = 2019;
  int months = 7;
  int days = 1;
  int hours = 0;
  int minutes = 0;
  int seconds = 0 ;
  if (Serial.available() > 0 ) {
    String datetimes = Serial.readString();
    if (datetimes.indexOf('/') > 0 ) {
      years = datetimes.substring(0, 4).toInt();
      months = datetimes.substring(5, 7).toInt();
      days = datetimes.substring(8, 10).toInt();
      hours = datetimes.substring(11, 13).toInt();
      minutes = datetimes.substring(14, 16).toInt();
      seconds = datetimes.substring(17, 19).toInt();
      rtc.adjust(DateTime(years, months, days, hours, minutes, seconds));
      mode = 0;
      Serial.println("RTC adjusted");
      showmenu();
    }

  }
  //  showmenu();
}

void checkModeInput(int input) {
  switch (input) {
    case 0:
      break;
    case 1:
      Serial.print("Mode >> ");
      Serial.println("Manual input Azimuth, Elevation and Time");
      Serial.println("Please input follow this format az,el# : ex. 180,30# ");
      mode = 1;
      break;
    case 2:
      Serial.print("Mode >> ");
      Serial.println("Read task from SDCard");
      mode = 2;
      break;
    case 3:
      Serial.print("Mode >> ");
      Serial.println("System check");
      systemcheck();
      showmenu();
      break;
    case 4:
      Serial.print("Mode >> ");
      Serial.println("Show current task");
      showTask();
      showmenu();
      break;
    case 5:
      Serial.print("Mode >> ");
      Serial.println("Remove current task");
      removeTask();
      showmenu();
      break;
    case 6:
      Serial.print("Mode >> ");
      Serial.println("Adjust datetime format YYYY/MM/DD hh:mm:ss");
      mode = 6;
      adjustdatetime();
      break;
    case 7:
      Serial.print("Mode >> ");
      Serial.println("Caribration Yaesu G5500.");
      mode = 7;
      break;
    default:
      Serial.println("invalid input please input again");
      break;
  }
}
void assigndata() {
  if (Serial.available() > 0 ) {
    tempPayload = Serial.readString();
    if (tempPayload.indexOf('#') > 0) {
      faz = tempPayload.substring(0, tempPayload.indexOf(',')).toInt();
      fel = tempPayload.substring(tempPayload.indexOf(',') + 1, tempPayload.indexOf('#')).toInt();
      rotor.setAzEl(faz, fel);
      faz = 0;
      fel = 0;
      mode = 0;
      showmenu();
    }
    else {
      Serial.println("invalid input");
    }
    Serial.println();
  }

}
void caribration() {
  switch (stepCaribration) {
    case 0 :
      Serial.println("Please rotate Azimuth,Elevation to 0,0 degrees. Then put 'OK' and send");
      stepCaribration = 1;
      rotor.setAzEl(0, 0);
      break;
    case 1 :
      if (Serial.available() > 0) {
        String putdata = Serial.readString();
        Serial.println(putdata);
        if (putdata.indexOf('K') > 0 ) {
          rotor.caribrateZero();
          stepCaribration = 2;
        }
        else {
          Serial.println("Wrong answer.");
        }
      }
      break;
    case 2 :
      Serial.println("Please rotate Azimuth,Elevation to 360,180 degrees. Then put 'OK' and send");
      stepCaribration = 3;
      rotor.setAzEl(360, 180);
      break;
    case 3 :
      if (Serial.available() > 0) {
        String putdata = Serial.readString();
        if (putdata.indexOf('K') > 0 ) {
          rotor.caribrateMax();
          stepCaribration = 4;
        }
        else {
          Serial.println("Wrong answer.");
        }
      }
      break;
    case 4 :
      rotor.caribrateResolution();
      stepCaribration = 0;
      mode = 0;
      Serial.print("Maximum Azimuth = ");
      Serial.println(rotor._maxAzPoint);
      Serial.print("Minimum Azimuth = ");
      Serial.println(rotor._zeroAzPoint);
      Serial.print("Maximum Elevation = ");
      Serial.println(rotor._maxElPoint);
      Serial.print("Minimum Elevation = ");
      Serial.println(rotor._zeroElPoint);
      Serial.print("Resolution of Azimuth, Elevation = ");
      Serial.print(rotor._azRes);
      Serial.print(", ");
      Serial.println(rotor._elRes);
      Serial.println("Caribration successfully.");
      showmenu();
      break;
  }


}
void loop() {
  if (Serial.available() > 0 && mode != 1 && mode != 6 && mode != 7) {
    checkModeInput(Serial.parseInt());
  }
  if (mode == 7) {
    caribration();
  }
  if (mode == 2 ) {
    rotation();
  }
  if (mode ==1) {
    assigndata();
  }
  if (mode == 6) {
    adjustdatetime();
  }
  if (getCurrentTime() > LOS && LOS != "") {
    removeTask();
  }

}

