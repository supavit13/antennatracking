import pandas as pd
import re
from tkinter import *
from tkinter import filedialog,messagebox

E1 = None
v = None
def converting():
    try:

        filename = E1.get()
        arr = []
        data = pd.read_csv(filename,header = None)
        print(len(data.loc[:,0].values))

        stringrow = ""
        for row in range(1,len(data.loc[:,0].values)):
            stringrow += data.loc[row,0]+" "+data.loc[row,1]+" "+data.loc[row,2]


        regex = r"(\d{2}:\d{2}:\d{2}\.\d+\s*\-?\d{1,3}\.\d+\s*\-?\d{1,3}\.\d+)"

            
        matches = re.finditer(regex, stringrow, re.MULTILINE)
        files = open("test.txt",'w')
        files = open("test.txt",'a+')
        for matchNum, match in enumerate(matches, start=1):
            schema = match.group()
            schema = re.findall(r'\S+',schema)
            if int(float(schema[2])) < 5:
                continue
            schema = str(int(float(schema[1])))+","+str(int(float(schema[2])))+","+str(int(schema[0][:2])+7)+schema[0][2:8]+"#"
            files.write(schema+"\n")    
            # break
        files.write("EOF")
        files.close()
        messagebox.showinfo( "parsing", "Successfully")
    except Exception as e:
        messagebox.showerror( "parsing", e)

def browsefunc():
    filename = filedialog.askopenfilename()
    v.set(filename)


window = Tk()
window.title("CSV file Parsing for yaesu-G5500")
L1 = Label(window, text="File Name")
L1.pack( side = LEFT)
v = StringVar()

B1 = Button(window,text="Submit",command=converting)
B1.pack(side=RIGHT)
browsebutton = Button(window, text="Browse", command=browsefunc)
browsebutton.pack(side=RIGHT)
E1 = Entry(window, bd =5,textvariable=v,state='disabled')
E1.pack(side = RIGHT)
window.resizable(False, False)
window.mainloop()
# filename = input("Enter filename >> ")

