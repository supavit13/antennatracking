/*G5500.h - Library for interfacing with the Yaesu G5500 roator
  controller.  Note that no particular language support is implied,
  as that is left to the user to implement.*/

// calculate resolution by 1/((x-y)/degree) ex. 1/((863 - 14)/360)
#ifndef G5500_h
#define G5500_h

#include "Arduino.h"

class G5500
{
  public:
    G5500();
    void setAzEl (int azimuth, int elevation);
    void setAz (int azimuth);
    void setEl (int elevation);
    void caribrateMax();
    void caribrateZero();
    void caribrateResolution();
    int getAz ();
    int getEl ();
    int getAzDegrees ();
    int getElDegrees ();
    int getAzDummy (int azimuth);
    void Move2PosAz (int azimuth);

  public:
    int _upPin = 11;
    int _downPin = 10;
    int _eastPin = 8;
    int _westPin = 9;
    int _azSensePin = A1;
    int _elSensePin = A0;
    int LEDAz_P = 7;
    int LEDAz_M = 6;
    int LEDEl_P = 5;
    int LEDEl_M = 3;
    //        //All of the following are determined experimentally
    //        const int _zeroAzPoint  = 7;
    //        const int _maxAzPoint   = 1023;
    //        const int _zeroElPoint = 0;
    //        const int _maxElPoint = 1020;
    //        const float _elRes    = 0.17665;
    //        const float _azRes    = 0.4228;
    //        //Set for ~2 deg dead zones to avoid chattering the motors
    //        const int _azDeadZone = 2;
    //        const int _elDeadZone = 10;
    //All of the following are determined experimentally
    const int _accuracy = 1;
    const int _defaultZeroAzPoint = 7;
    int _zeroAzPoint  = 14;
    int _maxAzPoint   = 1023;
    int _zeroElPoint = 0;
    int _maxElPoint = 1023;
    //    const float _elRes    = 0.1757;
    float _elRes    = 0.17595307917;
    //    const float _azRes    = 0.4394;
    float _azRes    = 0.423032;
    //Set for ~2 deg dead zones to avoid chattering the motors
    const int _azDeadZone = 2;
    const int _elDeadZone = 10;
};

#endif

