/*G5500.cpp - Library for interfacing with the Yaesu G5500 roator
  controller.  Note that no particular language support is implied,
  as that is left to the user to implement.  David Patterson,
  22-Mar-2015*/

#include "G5500.h"
#include "Arduino.h"


G5500::G5500() //constructor; basically, just set up the pins, turn them off
{
  pinMode(_upPin, OUTPUT);
  pinMode(_downPin, OUTPUT);
  pinMode(_eastPin, OUTPUT);
  pinMode(_westPin, OUTPUT);

  pinMode(LEDAz_P, OUTPUT);
  pinMode(LEDAz_M, OUTPUT);
  pinMode(LEDEl_P, OUTPUT);
  pinMode(LEDEl_M, OUTPUT);
  digitalWrite(_upPin, HIGH);
  digitalWrite(_downPin, HIGH);
  digitalWrite(_eastPin, HIGH);
  digitalWrite(_westPin, HIGH);
}

void G5500::setAzEl (int azimuth, int elevation)
{
  G5500::setAz(azimuth);
  G5500::setEl(elevation);
}

int G5500::getAzDummy(int azimuth)
{
  return (float(azimuth) / _azRes) + 0.5 + _zeroAzPoint;
}
void G5500::setAz(int azimuth)
{
  int sizeQ = 20;
  int queue[sizeQ] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int c = 0;
  int sum = 0;
  int azADC = (float(azimuth) / _azRes) + _zeroAzPoint;
  if (azADC > _maxAzPoint) azADC = _maxAzPoint;
  if (azADC < _zeroAzPoint) azADC = _zeroAzPoint;
  int azInd = G5500::getAz();
  if (abs(azADC - azInd) <= _azDeadZone) return;
  if (azInd == azADC) return;
  digitalWrite(_eastPin, LOW);
  digitalWrite(_westPin, LOW);
  while (1 )
    //  while (azInd != azADC )
  {
    if (c == sizeQ) {
      sum -= queue[0];
      for (int i = 0; i < sizeQ - 1; i++) {
        queue[i] = queue[i + 1];
      }
      queue[sizeQ - 1] = azInd;
      sum += azInd;
      c = sizeQ - 1;
    } else {
      queue[c] = azInd;
      sum += azInd;
    }
    c++;
    //    Serial.print("current average Az : ");
    //    Serial.println(sum / c);
    if (sum / c == azADC) break;
    if ((sum / c) + _accuracy == azADC) break;
    if ((sum / c) - _accuracy == azADC) break;
    if (azInd < azADC)
    {
      digitalWrite(_eastPin, HIGH);

      digitalWrite(LEDAz_P, HIGH);
    }
    else if (azInd > azADC)
    {
      digitalWrite(_westPin, HIGH);
      digitalWrite(LEDAz_M, HIGH);
    }

    azInd = G5500::getAz();
    if (sum / c == azADC) break;
    Serial.print("current Az : ");
    Serial.println(azInd);
    Serial.print("specific Az : ");
    Serial.println(azADC);
  }
  Serial.print("current average Az : ");
  Serial.println(sum / c);
  digitalWrite(LEDAz_P, LOW);
  digitalWrite(LEDAz_M, LOW);
  digitalWrite(_eastPin, HIGH);
  digitalWrite(_westPin, HIGH);
}

void G5500::setEl(int elevation)
{
  int sizeQ = 20;
  int queue[sizeQ] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int c = 0;
  int sum = 0;
  int elADC = (float(elevation) / _elRes) + _zeroElPoint;
  if (elADC > _maxElPoint) elADC = _maxElPoint;
  if (elADC < _zeroElPoint) elADC = _zeroElPoint;
  int elInd = G5500::getEl();
  if (abs(elADC - elInd) <= _elDeadZone) return;
  if (elInd == elADC) return;
  digitalWrite(_upPin, LOW);
  digitalWrite(_downPin, LOW);
  while (1)
    //  while (elInd != elADC)
  {
    if (c == sizeQ) {
      sum -= queue[0];
      for (int i = 0; i < sizeQ - 1; i++) {
        queue[i] = queue[i + 1];
      }
      queue[sizeQ - 1] = elInd;
      sum += elInd;
      c = sizeQ - 1;
    } else {
      queue[c] = elInd;
      sum += elInd;
    }
    c++;

    if (sum / c == elADC) break;
    if ((sum / c) + _accuracy == elADC) break;
    if ((sum / c) - _accuracy == elADC) break;
    if (elInd < elADC)
    {
      Serial.println("_downPin");
      digitalWrite( _downPin , HIGH);
      digitalWrite(LEDEl_P, HIGH);
    }
    else if (elInd > elADC)
    {
      Serial.println("_upPin");
      digitalWrite(_upPin, HIGH);
      digitalWrite(LEDEl_M, HIGH);

    }
    elInd = G5500::getEl();
    if (sum / c == elADC) break;
    Serial.print("current El : ");
    Serial.println(elInd);
    Serial.print("specific El : ");
    Serial.println(elADC);
  }
  Serial.print("current average El : ");
  Serial.println(sum / c);
  digitalWrite(LEDEl_P, LOW);
  digitalWrite(LEDEl_M, LOW);
  digitalWrite(_upPin, HIGH);
  digitalWrite(_downPin, HIGH);
}

int G5500::getAz()
{
  return analogRead(_azSensePin);
}

int G5500::getEl()
{
  return analogRead(_elSensePin);
}

int G5500::getAzDegrees()
{
  int azInd = G5500::getAz() - 7;
  int azimuth = float(azInd) * _azRes;
  if (azimuth < 0) azimuth = 0;
  else if (azimuth > 450) azimuth = 450;
  return azimuth;
}

int G5500::getElDegrees()
{
  int elInd = G5500::getEl();
  int elevation = float(elInd) * _elRes;
  if (elevation < 0) elevation = 0;
  else if (elevation > 180) elevation = 180;
  return elevation;
}
void G5500::caribrateMax()
{
  _maxAzPoint = G5500::getAz(); 
  _maxElPoint = G5500::getEl();
}
void G5500::caribrateZero()
{
  _zeroAzPoint = G5500::getAz();
  _zeroElPoint = G5500::getEl();
}
void G5500::caribrateResolution()
{
  _azRes = 1.0/(( float(_maxAzPoint) - float(_zeroAzPoint))/360.0);
  _elRes = 1.0/(( float(_maxElPoint) - float(_zeroElPoint))/180.0);
}

